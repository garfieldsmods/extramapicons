# Extra Map Icons

SP & MP Compatible. Provides new markers, and some new colours to be used both in Eden, and in game.

**Features**

  - 6 different unit markers, for BLUFOR, Independent and OPFOR
  - 10 different markers for use on the map by players
  - 15 different colours usable both in Eden, and in game.
