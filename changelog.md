v1.6.0
+ Added two new markers
  - Passage Point
  - Outpost
+ Fixed wing and Rotary UAV markers
+ Signals

v1.5.1
+ Modified incorrect file paths so the mod actually works (Thanks for the callout Vulture)

v1.5

added
+ Armoured Electronic Warfare units
+ Armoured Engineer units
+ Electronic Warfare Jammer units
+ Combined Arms

Updated

+ Refreshed PBO structure to make it easier to manage.

v1.4

added
+ Airborne unit marker
+ Air Assault unit marker
+ SAM unit marker
+ Forward Observer marker

updated
+ Updated class names for best practice
+ Backend build process updated



v1.3

updated
+ Thickened lines on some markers (Thanks for the feedback Bife)


v1.2

added
+ Booby Trap marker
+ Rally Point Marker
+ Start Point Marker
+ Mine Marker
+ AP Mine Marker
+ AT Mine Marker
+ Wide Area Marker


v1.1

added
+ Independent unit markers added

updated
+ Blufor IFV marker
+ Blufor Armoured Recce marker
