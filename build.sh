saveIFS="$IFS"
IFS=$'\n'
addons=( $(find ./addons -maxdepth 1 -mindepth 1 -type d -printf '%f\n') )
IFS="$saveIFS"
mkdir -p "@EMI/addons"
for f in "${addons[@]}"; do
    echo "addons/$f"
    armake build "addons/$f" "@EMI/addons/$f.pbo"
done
