class CfgPatches
{
  class CfgMarkers
  {
    units[]         = {};
    weapons[]       = {};
    requiredVersion = 1.96;
    requiredAddons[]= {"A3_Data_F_AoW_Loadorder", "A3_Data_F_Mod_Loadorder"};
    author          = "Garfield";
  };
};


class CfgMarkers
{
  class emi_o_IFV
    {
      name    =     "IFV";
      icon    =     "\ExtraMapIconsOpfor\images\o_IFV.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_MLRS
    {
      name    =     "MLRS";
      icon    =     "\ExtraMapIconsOpfor\images\o_MLRS.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_Rec_Arm_Cav
    {
      name    =     "Recon Armoured Cavalry";
      icon    =     "\ExtraMapIconsOpfor\images\o_Rec_Arm_Cav.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_VSTOL
    {
      name    =     "V/STOL";
      icon    =     "\ExtraMapIconsOpfor\images\o_VSTOL.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_Drone_fix
    {
      name    =     "Fixed Wing UAV";
      icon    =     "\ExtraMapIconsOpfor\images\o_UAV_fixed.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_Drone_rot
    {
      name    =     "Rotary UAV";
      icon    =     "\ExtraMapIconsOpfor\images\o_UAV_rotary.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_signal
    {
      name    =     "Signals";
      icon    =     "\ExtraMapIconsOpfor\images\o_Signal.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_Anti_Tank
    {
      name    =     "Anti Tank";
      icon    =     "\ExtraMapIconsOpfor\images\o_Anti_Tank.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_CBRN
    {
      name    =     "CBRN";
      icon    =     "\ExtraMapIconsOpfor\images\o_CBRN.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_infAirAssault
    {
      name    =     "Infantry Air Assualt";
      icon    =     "\ExtraMapIconsOpfor\images\o_Inf_Air_Assault.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_infAirborne
    {
      name    =     "Airborne Infantry";
      icon    =     "\ExtraMapIconsOpfor\images\o_Inf_Airborne.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_SAM
    {
      name    =     "SAM";
      icon    =     "\ExtraMapIconsOpfor\images\o_SAM.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_EWJ
    {
      name    =     "EW Jammer";
      icon    =     "\ExtraMapIconsOpfor\images\o_EW_Jam.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_Arm_EW
    {
      name    =     "Armoured EW";
      icon    =     "\ExtraMapIconsOpfor\images\o_Armoured_EW.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_Arm_Eng
    {
      name    =     "Armoured Engineers";
      icon    =     "\ExtraMapIconsOpfor\images\o_Armoured_Engineer.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };

  class emi_o_CombArms
    {
      name    =     "Combined Arms";
      icon    =     "\ExtraMapIconsOpfor\images\o_CombinedArms.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_OPFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_OPFOR_R',0])","(profilenamespace getvariable ['Map_OPFOR_G',1])","(profilenamespace getvariable ['Map_OPFOR_B',1])","(profilenamespace getvariable ['Map_OPFOR_A',0.8])"};
    };
};
