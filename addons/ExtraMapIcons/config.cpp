class CfgPatches
{
  class CfgMarkers
  {
    units[]         = {};
    weapons[]       = {};
    requiredVersion = 1.96;
    requiredAddons[]= {"A3_Data_F_AoW_Loadorder", "A3_Data_F_Mod_Loadorder"};
    author          = "Garfield";
  };
};


class CfgMarkers
{
  class  emi_bridgeCrossing
    {
      name    =    "Bridge Crossing";
      icon    =    "\ExtraMapIcons\images\bridgeCrossing.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_fordCrossing
    {
      name    =    "Ford Crossing";
      icon    =    "\ExtraMapIcons\images\fordCrossing.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_mainAttackDir
    {
      name    =     "Main Attack Direction";
      icon    =     "\ExtraMapIcons\images\mainAttackDir.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_supportAttackDir
    {
      name    =     "Support Attack Direction";
      icon    =     "\ExtraMapIcons\images\supportAttackDir.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_phaseLineLeft
    {
      name    =     "Phase Line Left";
      icon    =     "\ExtraMapIcons\images\phaseLineLeft.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_phaseLineRight
    {
      name    =     "Phase Line Right";
      icon    =     "\ExtraMapIcons\images\phaseLineRight.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_supportByFire
    {
      name    =     "Support By Fire";
      icon    =     "\ExtraMapIcons\images\supportByFire.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_targetRefPoint
    {
      name    =     "Target Reference Point";
      icon    =     "\ExtraMapIcons\images\targetRefPoint.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_marker
    {
      name    =     "Marker";
      icon    =     "\ExtraMapIcons\images\marker.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_marker_ckp
    {
      name    =     "Checkpoint";
      icon    =     "\ExtraMapIcons\images\marker_ckp.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_marker_crp
    {
      name    =     "Rally Point";
      icon    =     "\ExtraMapIcons\images\marker_rp.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_marker_pp
    {
      name    =     "Passage Point";
      icon    =     "\ExtraMapIcons\images\marker_pp.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_marker_sp
    {
      name    =     "Start Point";
      icon    =     "\ExtraMapIcons\images\marker_sp.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_marker_outpost
    {
      name    =     "Outpost";
      icon    =     "\ExtraMapIcons\images\marker_outpost.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_mine
    {
      name    =     "Mine";
      icon    =     "\ExtraMapIcons\images\mine.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_mine_AP
    {
      name    =     "Anti Personel Mine";
      icon    =     "\ExtraMapIcons\images\mine_AP.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_mine_AT
    {
      name    =     "Anti Tank Mine";
      icon    =     "\ExtraMapIcons\images\mine_AT.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_mine_WA
    {
      name    =     "Wide Area Mine";
      icon    =     "\ExtraMapIcons\images\mine_WA.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_boobyTrap
    {
      name    =     "Booby Trap";
      icon    =     "\ExtraMapIcons\images\boobyTrap.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };

  class emi_forwardObserver
    {
      name    =     "Forward Observer Pos";
      icon    =     "\ExtraMapIcons\images\ForwardObserverPos2.paa";
      shadow  =     1;
      scope   =     2;
      markerClass = "draw";
      size    =     32;
      color[] =     {0,0,0,1};
    };
};
