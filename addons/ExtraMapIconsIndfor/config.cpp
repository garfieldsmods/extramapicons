class CfgPatches
{
  class CfgMarkers
  {
    units[]         = {};
    weapons[]       = {};
    requiredVersion = 1.96;
    requiredAddons[]= {"A3_Data_F_AoW_Loadorder", "A3_Data_F_Mod_Loadorder"};
    author          = "Garfield";
  };
};


class CfgMarkers
{
  class emi_n_IFV
    {
      name    =     "IFV";
      icon    =     "\ExtraMapIconsIndfor\images\n_IFV.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     1;
      color[]   =   {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_MLRS
    {
      name    =     "MLRS";
      icon    =     "\ExtraMapIconsIndfor\images\n_MLRS.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_Rec_Arm_Cav
    {
      name    =     "Recon Armoured Cavalry";
      icon    =     "\ExtraMapIconsIndfor\images\n_Rec_Arm_Cav.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_VSTOL
    {
      name    =     "V/STOL";
      icon    =     "\ExtraMapIconsIndfor\images\n_VSTOL.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_Drone_fix
    {
      name    =     "Fixed Wing UAV";
      icon    =     "\ExtraMapIconsIndfor\images\n_UAV_fixed.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_Drone_rot
    {
      name    =     "Rotary UAV";
      icon    =     "\ExtraMapIconsIndfor\images\n_UAV_rotary.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_signal
    {
      name    =     "Signals";
      icon    =     "\ExtraMapIconsIndfor\images\n_Signal.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_Anti_Tank
    {
      name    =     "Anti Tank";
      icon    =     "\ExtraMapIconsIndfor\images\n_Anti_Tank.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_CBRN
    {
      name    =     "CBRN";
      icon    =     "\ExtraMapIconsIndfor\images\n_CBRN.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_infAirAssault
    {
      name    =     "Infantry Air Assualt";
      icon    =     "\ExtraMapIconsIndfor\images\n_Inf_Air_Assault.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_infAirborne
    {
      name    =     "Airborne Infantry";
      icon    =     "\ExtraMapIconsIndfor\images\n_Inf_Airborne.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_SAM
    {
      name    =     "SAM";
      icon    =     "\ExtraMapIconsIndfor\images\n_SAM.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_EWJ
    {
      name    =     "EW Jammer";
      icon    =     "\ExtraMapIconsIndfor\images\n_EW_Jam.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_Arm_EW
    {
      name    =     "Armoured EW";
      icon    =     "\ExtraMapIconsIndfor\images\n_Armoured_EW.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_Arm_Eng
    {
      name    =     "Armoured Engineers";
      icon    =     "\ExtraMapIconsIndfor\images\n_Armoured_Engineer.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

  class emi_n_CombArms
    {
      name    =     "Combined Arms";
      icon    =     "\ExtraMapIconsIndfor\images\n_CombinedArms.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_Independent";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_Independent_R',0])","(profilenamespace getvariable ['Map_Independent_G',1])","(profilenamespace getvariable ['Map_Independent_B',1])","(profilenamespace getvariable ['Map_Independent_A',0.8])"};
    };

};
