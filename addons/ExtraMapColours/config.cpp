class CfgPatches
{
  class CfgMarkerColors
  {
    units[]         = {};
    weapons[]       = {};
    requiredVersion = 1.96;
    requiredAddons[]= {"A3_Data_F_AoW_Loadorder", "A3_Data_F_Mod_Loadorder"};
    author          = "Garfield";
  };
};


class CfgMarkerColors
{
    class emc_brown
    {
      name        = "Brown";
      scope       = 2;
      color[]     = {0.5,0.25,0,1};
    };

    class emc_cyan
    {
      name        = "Cyan";
      scope       = 2;
      color[]     = {0,1,1,1};
    };

    class emc_beige
    {
      name        = "Beige";
      scope       = 2;
      color[]     = {0.96,0.96,0.86,1};
    };

    class emc_lavender
    {
      name        = "Lavender";
      scope       = 2;
      color[]     = {0.9,0.9,0.98,1};
    };

    class emc_peach
    {
      name        = "Peach";
      scope       = 2;
      color[]     = {1,0.85,0.72,1};
    };

    class emc_teal
    {
      name        = "Teal";
      scope       = 2;
      color[]     = {0,0.5,0.5,1};
    };

    class emc_turquoise
    {
      name        = "Turquoise";
      scope       = 2;
      color[]     = {0.25,0.87,0.81,1};
    };

    class emc_slate
    {
      name        = "Slate";
      scope       = 2;
      color[]     = {0.43,0.5,0.56,1};
    };

    class emc_silver
    {
      name        = "Silver";
      scope       = 2;
      color[]     = {0.75,0.75,0.75,1};
    };

    class emc_orange
    {
      name        = "Orange";
      scope       = 2;
      color[]     = {1,0.53,0,1};
    };

    class emc_brg
    {
      name        = "British Racing Green";
      scope       = 2;
      color[]     = {0,0.26,0.15,1};
    };

    class emc_lilac
    {
      name        = "Lilac";
      scope       = 2;
      color[]     = {0.53,0,1,1};
    };

    class emc_khaki
    {
      name        = "Khaki";
      scope       = 2;
      color[]     = {0.74,0.72,0.42,1};
    };

    class emc_olive
    {
      name        = "Olive";
      scope       = 2;
      color[]     = {0.5,0.5,0,1};
    };

    class emc_hpink
    {
      name        = "Hot Pink";
      scope       = 2;
      color[]     = {1,0.41,0.71,1};
    };
};
