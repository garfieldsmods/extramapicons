class CfgPatches
{
  class CfgMarkers
  {
    units[]         = {};
    weapons[]       = {};
    requiredVersion = 1.96;
    requiredAddons[]= {"A3_Data_F_AoW_Loadorder", "A3_Data_F_Mod_Loadorder"};
    author          = "Garfield";
  };
};


class CfgMarkers
{
  class emi_b_IFV
    {
      name    =     "IFV";
      icon    =     "\ExtraMapIconsBlufor\images\b_IFV.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     1;
      color[]   =   {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_MLRS
    {
      name    =     "MLRS";
      icon    =     "\ExtraMapIconsBlufor\images\b_MLRS.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_Rec_Arm_Cav
    {
      name    =     "Recon Armoured Cavalry";
      icon    =     "\ExtraMapIconsBlufor\images\b_Rec_Arm_Cav.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_VSTOL
    {
      name    =     "V/STOL";
      icon    =     "\ExtraMapIconsBlufor\images\b_VSTOL.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_Drone_fix
    {
      name    =     "Fixed Wing UAV";
      icon    =     "\ExtraMapIconsBlufor\images\b_UAV_fixed.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_Drone_rot
    {
      name    =     "Rotary UAV";
      icon    =     "\ExtraMapIconsBlufor\images\b_UAV_rotary.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_signal
    {
      name    =     "Signals";
      icon    =     "\ExtraMapIconsBlufor\images\b_Signal.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_Anti_Tank
    {
      name    =     "Anti Tank";
      icon    =     "\ExtraMapIconsBlufor\images\b_Anti_Tank.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_CBRN
    {
      name    =     "CBRN";
      icon    =     "\ExtraMapIconsBlufor\images\b_CBRN.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_infAirAssault
    {
      name    =     "Infantry Air Assualt";
      icon    =     "\ExtraMapIconsBlufor\images\b_Inf_Air_Assault.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_infAirborne
    {
      name    =     "Airborne Infantry";
      icon    =     "\ExtraMapIconsBlufor\images\b_Inf_Airborne.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_SAM
    {
      name    =     "SAM";
      icon    =     "\ExtraMapIconsBlufor\images\b_SAM.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_EWJ
    {
      name    =     "EW Jammer";
      icon    =     "\ExtraMapIconsBlufor\images\b_EW_Jam.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_Arm_EW
    {
      name    =     "Armoured EW";
      icon    =     "\ExtraMapIconsBlufor\images\b_Armoured_EW.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_Arm_Eng
    {
      name    =     "Armoured Engineers";
      icon    =     "\ExtraMapIconsBlufor\images\b_Armoured_Engineer.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };

  class emi_b_CombArms
    {
      name    =     "Combined Arms";
      icon    =     "\ExtraMapIconsBlufor\images\b_CombinedArms.paa";
      shadow  =     0;
      scope   =     2;
      markerClass = "NATO_BLUFOR";
      size    =     29;
      showEditorMarkerColor = 1;
      side    =     0;
      color[] =     {"(profilenamespace getvariable ['Map_BLUFOR_R',0])","(profilenamespace getvariable ['Map_BLUFOR_G',1])","(profilenamespace getvariable ['Map_BLUFOR_B',1])","(profilenamespace getvariable ['Map_BLUFOR_A',0.8])"};
    };
};
